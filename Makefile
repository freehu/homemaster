# HomeMaster 客户端程序编译

all:
	GOOS=linux GOARCH=mipsle go build -o build/files/tmp/homemaster -ldflags "-s -w"

ko:
	@cd driver && make
	@mv driver/homemaster.ko build/files/lib/modules/3.18.29/

remote:
	scp -r build/files/tmp/homemaster root@homemaster.local:/tmp/

home:
	scp -r build/files/tmp/homemaster root@home.local:/tmp/

clean:
	rm -rf build/files/tmp/homemaster