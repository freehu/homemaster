package client

import (
	"encoding/json"
	"adai.design/homemaster/container/accessory"
	"adai.design/homemaster/container/service"
	"adai.design/homemaster/container/characteristic"
	"adai.design/homemaster/container"
	"adai.design/homemaster/log"
	"time"
)

func notifyServer(client *Client, a *accessory.Accessory, s *service.Service, c *characteristic.Characteristic) {
	if client.notify != notifyEnable {
		return
	}

	chars := []*Characteristic{{
		AccessoryID: a.UUID,
		ServiceId: s.ID,
		CharacteristicId: c.Id,
		Value: c.Value,
	}}

	data, _ := json.Marshal(chars)

	msg := &Message{
		Path: MsgPathCharacteristic,
		Method: MsgMethodPut,
		Data: data,
	}

	client.writeMessage(msg)
}

func notifyStart(client *Client) {
	load := func() {
		container := container.GetContainer()
		for _, a := range container.Accessories {
			func(a *accessory.Accessory) {
				for _, s := range a.Services {
					func(a *accessory.Accessory, s *service.Service) {
						for _, c := range s.Characteristics {
							onChange := func(c *characteristic.Characteristic, new, old interface{}) {
								//log.Info("update aid(%s %d %d)", a.UUID, s.ID, c.Id)
								notifyServer(client, a, s, c)
							}
							c.OnValueUpdate(onChange)
						}
					}(a, s)
				}
			}(a)
		}
	}

	master := container.GetContainer()
	// 容器设备列表发生变化造成的版本变化
	change := func(new, old int) {
		load()
		info := map[string]interface{}{
			"version":  container.GetContainer().Version,
			"firmware": "0.0.1",
			"date":     time.Now().Add(time.Hour * 8).Format("2006/01/02 15:04:05"),
		}

		data, _ := json.Marshal(info)
		put := &Message{
			Path:   MsgPathContainer,
			Method: MsgMethodPut,
			Data:   data,
			State:  "ok",
		}
		data, _ = json.Marshal(put)
		log.Debug("put (%s)", string(data))

		if client.notify == notifyEnable {
			client.writeMessage(put)
		}
	}

	master.OnVersionChange(change)
	load()
}