package characteristic

import "time"

// 屏幕亮度
type Brightness struct {
	*Int
}

func NewBrightness() *Brightness {
	char := NewInt(TypeScreenBrightness)
	char.Format = FormatUInt8
	char.MinValue = 0
	char.MaxValue = 8
	char.StepValue = 1
	char.SetValue(8)
	return &Brightness{char}
}

// 当前时间
type CurrentTime struct {
	*String
}

func NewCurrentTime() *CurrentTime {
	char := NewString(TypeCurrentTime)
	char.SetValue(time.Now().Format("15:04"))
	return &CurrentTime{char}
}

// 音量大小
type Volume struct {
	*Int
}

func NewVolume() *Volume {
	char := NewInt(TypeVolume)
	char.Format = FormatUInt8
	char.SetValue(0)

	char.MaxValue = 100
	char.MinValue = 0
	char.StepValue = 1
	char.Unit = UnitPercentage
	return &Volume{char}
}

// 音乐名称
type MusicName struct {
	*String
}

func NewMusicName() *MusicName {
	char := NewString(TypeMusicName)
	char.Format = FormatString
	char.SetValue("")
	return &MusicName{char}
}

// 当前温度
type CurrentTemperature struct {
	*Float
}

func NewCurrentTemperature() *CurrentTemperature {
	char := NewFloat(TypeCurrentTemperature)
	char.Format = FormatFloat
	char.SetValue(0)
	char.MinValue = -20.0
	char.MaxValue = 80.0
	char.StepValue = 0.1
	char.Unit = UnitCelsius
	return &CurrentTemperature{char}
}

// 当前相对湿度
type CurrentRelativeHumidity struct {
	*Int
}

func NewCurrentHumidity() *CurrentRelativeHumidity {
	char := NewInt(TypeCurrentRelativeHumidity)
	char.Format = FormatUInt8
	char.SetValue(0)
	char.MaxValue = 100
	char.MinValue = 0
	char.StepValue = 1
	char.Unit = UnitPercentage
	return &CurrentRelativeHumidity{char}
}

// 环境光照强度
type CurrentAmbientLightLevel struct {
	*Int
}

func NewCurrentAmbientLightLevel() *CurrentAmbientLightLevel {
	char := NewInt(TypeCurrentAmbientLightLevel)
	char.Format = FormatUInt32
	char.SetValue(0)
	char.MinValue = 0
	char.MaxValue = 100000
	char.Unit = UnitLux
	return &CurrentAmbientLightLevel{char}
}

// 环境不可见光强度
type CurrentAmbientInfraredLevel struct {
	*Int
}

func NewCurrentAmbientInfraredLevel() *CurrentAmbientInfraredLevel {
	char := NewInt(TypeCurrentAmbientInfraredLevel)
	char.Format = FormatUInt16
	char.SetValue(0)
	char.MinValue = 0
	char.MaxValue = 100000
	return &CurrentAmbientInfraredLevel{char}
}

// 接触传感器状态
const (
	ContactSensorStateContactDetected    = 0
	ContactSensorStateContactNotDetected = 1
)

type ContactSensorState struct {
	*Int
}

func NewContactSensorState() *ContactSensorState {
	char := NewInt(TypeContactSensorState)
	char.Format = FormatUInt8
	char.SetValue(ContactSensorStateContactNotDetected)
	return &ContactSensorState{char}
}

// 开关状态
type Switch struct {
	*Int
}

const (
	SwitchStateOff = 0
	SwitchStateOn  = 1
)

func NewOn() *Switch {
	char := NewInt(TypeOn)
	char.Format = FormatUInt8
	char.SetValue(0)
	return &Switch{char}
}

// 目标温度 (空调)
type TargetTemperature struct {
	*Float
}

func NewTargetTemperature() *TargetTemperature {
	char := NewFloat(TypeTargetTemperature)
	char.Format = FormatFloat
	char.SetValue(25)
	char.MinValue = 16
	char.MaxValue = 30
	char.StepValue = 1
	char.Unit = UnitCelsius
	return &TargetTemperature{char}
}

// 空调模式
// 自动\制冷\除湿\制热\送风
const (
	AirConditionerModeAuto             = 1
	AirConditionerModeCool             = 2
	AirConditionerModeDehumidification = 3
	AirConditionerModeHeat             = 4
	AirConditionerBlowing              = 5
)

type AirConditionerMode struct {
	*Int
}

func NewAirConditionerMode() *AirConditionerMode {
	char := NewInt(TypeAirConditionerMode)
	char.SetValue(AirConditionerModeAuto)
	char.Format = FormatUInt8
	return &AirConditionerMode{char}
}

// 空调风类型
// 自然风\上下摆风
const (
	AirConditionerRotationModeNatural = 1
	// 向下吹 向上吹 向上吹风 上下摆风
	AirConditionerRotationModeSwing = 2
	AirConditionerWindDirectLower   = 3
	AirConditionerWindDirectMiddle  = 4
	AirConditionerWindDirectAbove   = 5
)

type AirConditionerRotationMode struct {
	*Int
}

func NewAirConditionerRotationMode() *AirConditionerRotationMode {
	char := NewInt(TypeAirConditionerRotationMode)
	char.Format = FormatUInt8
	char.SetValue(AirConditionerRotationModeNatural)
	return &AirConditionerRotationMode{char}
}

// 风速
type AirConditionerWindSpeed struct {
	*Int
}

func NewAirConditionerWindSpeed() *AirConditionerWindSpeed {
	char := NewInt(TypeAirConditionerWindSpeed)
	char.Format = FormatUInt8
	char.SetValue(80)
	char.MinValue = 0
	char.MaxValue = 100
	char.StepValue = 25
	return &AirConditionerWindSpeed{char}
}

// 光的亮度
type LightLevel struct {
	*Int
}

func NewLightLevel() *LightLevel {
	char := NewInt(TypeCurrentLightLevel)
	char.Format = FormatUInt8
	char.SetValue(80)
	char.MinValue = 0
	char.MaxValue = 100
	char.StepValue = 10
	return &LightLevel{char}
}

// 光的颜色
type LightColor struct {
	*String
}

func NewLightColor() *LightColor {
	char := NewString(TypeCurrentLightColor)
	char.Format = FormatString
	char.SetValue("ffffff00")
	return &LightColor{char}
}

// 光的色温
type LightColorTemperature struct {
	*Int
}

func NewLightColorTemperature() *LightColorTemperature {
	char := NewInt(TypeCurrentLightColorTemperature)
	char.Format = FormatInt32
	char.SetValue(0)
	return &LightColorTemperature{char}
}

// 配件名称
type Name struct {
	*String
}

func NewName() *Name {
	char := NewString(TypeName)
	char.Format = FormatString
	char.SetValue("")
	return &Name{char}
}

// 配件型号
type Model struct {
	*String
}

func NewModel() *Model {
	char := NewString(TypeModel)
	char.Format = FormatString
	char.SetValue("")
	return &Model{char}
}

// 配件制造商
type Manufacturer struct {
	*String
}

func NewManufacturer() *Manufacturer {
	char := NewString(TypeManufacturer)
	char.Format = FormatString
	char.SetValue("")
	return &Manufacturer{char}
}

// 配件序列号
type SerialNumber struct {
	*String
}

func NewSerialNumber() *SerialNumber {
	char := NewString(TypeSerialNumber)
	char.Format = FormatString
	char.SetValue("")
	return &SerialNumber{char}
}

// 配件固件版本
type FirmwareRevision struct {
	*String
}

func NewFirmwareRevision() *FirmwareRevision {
	char := NewString(TypeFirmwareRevision)
	char.Format = FormatString
	char.SetValue("")
	return &FirmwareRevision{char}
}
