package service

import "adai.design/homemaster/container/characteristic"

// 配件基本描述信息
const (
	AccessoryInfoNameCharId             = 1
	AccessoryInfoManufacturerCharId     = 2
	AccessoryInfoModelCharId            = 3
	AccessoryInfoSerialNumberCharId     = 4
	AccessoryInfoFirmwareRevisionCharId = 5
)

type AccessoryInformation struct {
	*Service

	Name             *characteristic.Name
	Manufacturer     *characteristic.Manufacturer
	Model            *characteristic.Model
	SerialNumber     *characteristic.SerialNumber
	FirmwareRevision *characteristic.FirmwareRevision
}

func NewAccessoryInformation() *AccessoryInformation {
	sev := AccessoryInformation{}
	sev.Service = New(TypeAccessoryInformation)

	sev.Name = characteristic.NewName()
	sev.Name.Id = AccessoryInfoNameCharId
	sev.AddCharacteristic(sev.Name.Characteristic)

	sev.Manufacturer = characteristic.NewManufacturer()
	sev.Manufacturer.Id = AccessoryInfoManufacturerCharId
	sev.AddCharacteristic(sev.Manufacturer.Characteristic)

	sev.Model = characteristic.NewModel()
	sev.Model.Id = AccessoryInfoModelCharId
	sev.AddCharacteristic(sev.Model.Characteristic)

	sev.SerialNumber = characteristic.NewSerialNumber()
	sev.SerialNumber.Id = AccessoryInfoSerialNumberCharId
	sev.Service.AddCharacteristic(sev.SerialNumber.Characteristic)

	sev.FirmwareRevision = characteristic.NewFirmwareRevision()
	sev.FirmwareRevision.Id = AccessoryInfoFirmwareRevisionCharId
	sev.Service.AddCharacteristic(sev.FirmwareRevision.Characteristic)
	return &sev
}

// 接触式传感器
const (
	ContactSensorCharId = 1
)

type ContactSensor struct {
	*Service

	ContactSensorState *characteristic.ContactSensorState
}

func NewContactSensor() *ContactSensor {
	sev := ContactSensor{}
	sev.Service = New(TypeContactSensor)

	sev.ContactSensorState = characteristic.NewContactSensorState()
	sev.ContactSensorState.Id = ContactSensorCharId
	sev.Service.AddCharacteristic(sev.ContactSensorState.Characteristic)
	return &sev
}

// 温湿度传感器
const (
	HTSensorTemperatureCharId = 1
	HTSensorHumidityCharId    = 2
)

type HTSensor struct {
	*Service

	Temperature *characteristic.CurrentTemperature
	Humidity    *characteristic.CurrentRelativeHumidity
}

func NewHTSensor() *HTSensor {
	sev := HTSensor{}
	sev.Service = New(TypeHTSensor)

	sev.Temperature = characteristic.NewCurrentTemperature()
	sev.Temperature.Id = HTSensorTemperatureCharId
	sev.Service.AddCharacteristic(sev.Temperature.Characteristic)

	sev.Humidity = characteristic.NewCurrentHumidity()
	sev.Humidity.Id = HTSensorHumidityCharId
	sev.Service.AddCharacteristic(sev.Humidity.Characteristic)

	return &sev
}

// 光照强度传感器
const (
	LightSensorAmbientLightCharId    = 1
	LightSensorAmbientInfraredCharId = 2
)

type LightSensor struct {
	*Service

	Light    *characteristic.CurrentAmbientLightLevel
	Infrared *characteristic.CurrentAmbientInfraredLevel
}

func NewLightSensor() *LightSensor {
	sev := LightSensor{}
	sev.Service = New(TypeLightSensor)

	sev.Light = characteristic.NewCurrentAmbientLightLevel()
	sev.Light.Id = LightSensorAmbientLightCharId
	sev.Service.AddCharacteristic(sev.Light.Characteristic)

	sev.Infrared = characteristic.NewCurrentAmbientInfraredLevel()
	sev.Infrared.Id = LightSensorAmbientInfraredCharId
	sev.Service.AddCharacteristic(sev.Infrared.Characteristic)

	return &sev
}

// 空调
const (
	AirConditionOnCharId                  = 1
	AirConditionerModeCharId              = 2
	AirConditionerTargetTemperatureCharId = 3
	AirConditionerRotationModeCharId      = 4
	AirConditionerWindSpeedCharId         = 5
)

type AirConditioner struct {
	*Service

	On           *characteristic.Switch
	Temperature  *characteristic.TargetTemperature
	Model        *characteristic.AirConditionerMode
	RotationMode *characteristic.AirConditionerRotationMode
	WindSpeed    *characteristic.AirConditionerWindSpeed
}

func NewAirConditioner() *AirConditioner {
	sev := AirConditioner{}
	sev.Service = New(TypeAirConditioner)

	sev.On = characteristic.NewOn()
	sev.On.Id = AirConditionOnCharId
	sev.Service.AddCharacteristic(sev.On.Characteristic)

	sev.Temperature = characteristic.NewTargetTemperature()
	sev.Temperature.Id = AirConditionerTargetTemperatureCharId
	sev.Service.AddCharacteristic(sev.Temperature.Characteristic)

	sev.Model = characteristic.NewAirConditionerMode()
	sev.Model.Id = AirConditionerModeCharId
	sev.Service.AddCharacteristic(sev.Model.Characteristic)

	sev.RotationMode = characteristic.NewAirConditionerRotationMode()
	sev.RotationMode.Id = AirConditionerRotationModeCharId
	sev.Service.AddCharacteristic(sev.RotationMode.Characteristic)

	sev.WindSpeed = characteristic.NewAirConditionerWindSpeed()
	sev.WindSpeed.Id = AirConditionerWindSpeedCharId
	sev.Service.AddCharacteristic(sev.WindSpeed.Characteristic)

	return &sev
}

// 开关
const (
	SwitchOnCharId = 1
)

type Switch struct {
	*Service

	On *characteristic.Switch
}

func NewSwitch() *Switch {
	sev := Switch{}
	sev.Service = New(TypeSwitch)

	sev.On = characteristic.NewOn()
	sev.On.Id = SwitchOnCharId
	sev.AddCharacteristic(sev.On.Characteristic)

	return &sev
}

// 插座
const (
	OutletOnCharId = 1
)

type Outlet struct {
	*Service

	Switch *characteristic.Switch
}

func NewOutlet() *Outlet {
	sev := Outlet{}
	sev.Service = New(TypeOutlet)

	sev.Switch = characteristic.NewOn()
	sev.Switch.Id = OutletOnCharId
	sev.AddCharacteristic(sev.Switch.Characteristic)

	return &sev
}

// 闹钟
// 时间\屏幕亮度\闹钟音量
const (
	ClockTimeCharId       = 1
	ClockBrightnessCharId = 2
	ClockVolumeCharId     = 3
	ClockMusicName        = 4
)

type Clock struct {
	*Service

	Time       *characteristic.CurrentTime
	Brightness *characteristic.Brightness
	Volume     *characteristic.Volume
	Music      *characteristic.MusicName
}

func NewClock() *Clock {
	sev := Clock{}
	sev.Service = New(TypeClock)

	sev.Time = characteristic.NewCurrentTime()
	sev.Time.Id = ClockTimeCharId
	sev.Service.AddCharacteristic(sev.Time.Characteristic)

	sev.Brightness = characteristic.NewBrightness()
	sev.Brightness.Id = ClockBrightnessCharId
	sev.Service.AddCharacteristic(sev.Brightness.Characteristic)

	sev.Volume = characteristic.NewVolume()
	sev.Volume.Id = ClockVolumeCharId
	sev.Service.AddCharacteristic(sev.Volume.Characteristic)

	sev.Music = characteristic.NewMusicName()
	sev.Music.Id = ClockMusicName
	sev.Service.AddCharacteristic(sev.Music.Characteristic)

	return &sev
}

const (
	MotionDetectedCharId   = 1
	MotionLightLevelCharId = 2
)

type Motion struct {
	*Service

	OnOff     *characteristic.Switch
	Lightness *characteristic.CurrentAmbientLightLevel
}

func NewMotion() *Motion {
	sev := Motion{}
	sev.Service = New(TypeMotion)

	sev.OnOff = characteristic.NewOn()
	sev.OnOff.Id = MotionDetectedCharId
	sev.Service.AddCharacteristic(sev.OnOff.Characteristic)

	sev.Lightness = characteristic.NewCurrentAmbientLightLevel()
	sev.Lightness.Id = ClockBrightnessCharId
	sev.Service.AddCharacteristic(sev.Lightness.Characteristic)

	return &sev
}

const (
	LightSwitchCharId           = 1
	LightLevelCharId            = 2
	LightColorCharId            = 3
	LightColorTemperatureCharId = 4
)

type LightColor struct {
	*Service

	On          *characteristic.Switch
	Level       *characteristic.LightLevel
	Color       *characteristic.LightColor
	Temperature *characteristic.LightColorTemperature
}

func NewColorLight() *LightColor {
	sev := LightColor{}
	sev.Service = New(TypeLightColor)

	sev.On = characteristic.NewOn()
	sev.On.Id = LightSwitchCharId
	sev.Service.AddCharacteristic(sev.On.Characteristic)

	sev.Level = characteristic.NewLightLevel()
	sev.Level.Id = LightLevelCharId
	sev.Service.AddCharacteristic(sev.Level.Characteristic)

	sev.Color = characteristic.NewLightColor()
	sev.Color.Id = LightColorCharId
	sev.Service.AddCharacteristic(sev.Color.Characteristic)

	sev.Temperature = characteristic.NewLightColorTemperature()
	sev.Temperature.Id = LightColorTemperatureCharId
	sev.Service.AddCharacteristic(sev.Temperature.Characteristic)

	return &sev
}

type LightTemperatureColor struct {
	*Service

	On          *characteristic.Switch
	Level       *characteristic.LightLevel
	Temperature *characteristic.LightColorTemperature
}

func NewLightTemperatureColor() *LightTemperatureColor {
	sev := LightTemperatureColor{}
	sev.Service = New(TypeLightColorTemperature)

	sev.On = characteristic.NewOn()
	sev.On.Id = LightSwitchCharId
	sev.Service.AddCharacteristic(sev.On.Characteristic)

	sev.Level = characteristic.NewLightLevel()
	sev.Level.Id = LightLevelCharId
	sev.Service.AddCharacteristic(sev.Level.Characteristic)

	sev.Temperature = characteristic.NewLightColorTemperature()
	sev.Temperature.Id = LightColorTemperatureCharId
	sev.Service.AddCharacteristic(sev.Temperature.Characteristic)

	return &sev
}
