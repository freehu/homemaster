package main

import (
	"adai.design/homemaster/log"
	"adai.design/homemaster/coordinator"
)

func main() {
	log.Println("let's go!")
	coordinator.StartCoordinator()
	select {
	}
}

