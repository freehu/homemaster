package modules

import (
	"testing"
	"adai.design/homemaster/log"
)

func TestByte(t *testing.T) {
	x := 0xA6 & (0x01 << 7)
	log.Info("%x", x)
}

func TestHaierConditionerEncode(t *testing.T) {
	air := &haierAirConditioner{
		OnOff: true,
		Mode: 0x00,
		Temperature: 25,
	}

	_, err := air.encode()
	if err != nil {
		t.Fatal(err)
	}

	t.Logf("data=%x", air.buffer)
}





