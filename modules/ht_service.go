package modules

import (
	"adai.design/homemaster/container/service"
	"adai.design/homemaster/container"
)

const HTSensorSevId = 1

var htService = service.NewHTSensor()

func init() {
	container.AddService(htService.Service, HTSensorSevId)
	htService.SetName("HTSensor")
}
